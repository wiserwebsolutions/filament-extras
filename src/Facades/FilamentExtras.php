<?php

namespace FilamentExtras\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see \FilamentExtras\FilamentExtras
 */
class FilamentExtras extends Facade
{
    protected static function getFacadeAccessor()
    {
        return \FilamentExtras\FilamentExtras::class;
    }
}
