<?php

namespace FilamentExtras\Commands;

use Illuminate\Console\Command;

class FilamentExtrasCommand extends Command
{
    public $signature = 'filament-extras';

    public $description = 'My command';

    public function handle(): int
    {
        $this->comment('All done');

        return self::SUCCESS;
    }
}
