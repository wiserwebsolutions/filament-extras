<?php

namespace FilamentExtras\Tables\Actions;

use Filament\Actions\Concerns\CanCustomizeProcess;
use Filament\Support\Facades\FilamentIcon;
use Filament\Tables\Contracts\HasTable;
use Filament\Tables\Filters\TrashedFilter;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class ArchiveBulkAction extends \Filament\Tables\Actions\BulkAction
{
    use CanCustomizeProcess;

    public static function getDefaultName(): ?string
    {
        return 'archive';
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->label(__('filament-extras::archive.multiple.label'));

        $this->modalHeading(fn (): string => __('filament-extras::archive.multiple.modal.heading', ['label' => $this->getPluralModelLabel()]));

        $this->modalSubmitActionLabel(__('filament-extras::archive.multiple.modal.actions.archive.label'));

        $this->successNotificationTitle(__('filament-extras::archive.multiple.notifications.archived.title'));

        $this->color('warning');

        $this->icon('heroicon-s-archive-box');

        $this->requiresConfirmation();

        $this->modalIcon('heroicon-s-archive-box');

        // Checked to here





        $this->action(function (): void {
            $this->process(static fn (Collection $records) => $records->each(fn (Model $record) => $record->delete()));

            $this->success();
        });

        $this->deselectRecordsAfterCompletion();

        $this->hidden(function (HasTable $livewire): bool {
            $trashedFilterState = $livewire->getTableFilterState(TrashedFilter::class) ?? [];

            if (! array_key_exists('value', $trashedFilterState)) {
                return false;
            }

            if ($trashedFilterState['value']) {
                return false;
            }

            return filled($trashedFilterState['value']);
        });
    }
}
