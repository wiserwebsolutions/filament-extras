<?php

namespace FilamentExtras\Tables\Actions;

use Filament\Tables\Actions\ActionGroup;
use Filament\Tables\Actions\ViewAction;
use Filament\Tables\Actions\EditAction;
use Filament\Tables\Actions\RestoreAction;
use Filament\Tables\Actions\DeleteAction;
use Filament\Tables\Actions\ForceDeleteAction;

/**
 * @property array<Action | BulkAction> $actions
 */
class DefaultActionGroup extends ActionGroup
{
    /**
     * @param  array<StaticAction | ActionGroup>  $actions
     */
    public static function make(array $actions = []): static
    {
        $actions = array_merge(
            [
                ViewAction::make(),
                EditAction::make(),
                ArchiveAction::make(),
                // DeleteAction::make(), // Should check against record
                RestoreAction::make(),
                ForceDeleteAction::make(),
            ],
            $actions,
        );

        foreach($actions as $action) {
            $unique[get_class($action)] = $action;
        }

        $static = app(static::class, ['actions' => $unique]);
        $static->configure();

        return $static;
    }
}
