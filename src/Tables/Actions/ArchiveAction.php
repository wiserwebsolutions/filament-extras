<?php

namespace FilamentExtras\Tables\Actions;

use Filament\Actions\Concerns\CanCustomizeProcess;
use Filament\Support\Facades\FilamentIcon;
use Illuminate\Database\Eloquent\Model;

class ArchiveAction extends \Filament\Tables\Actions\Action
{
    use CanCustomizeProcess;

    public static function getDefaultName(): ?string
    {
        return 'archive';
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->label(__('filament-extras::archive.single.label'));

        $this->modalHeading(fn (): string => __('filament-extras::archive.single.modal.heading', ['label' => $this->getPluralModelLabel()]));

        $this->modalSubmitActionLabel(__('filament-extras::archive.single.modal.actions.archive.label'));

        $this->successNotificationTitle(__('filament-extras::archive.single.notifications.archived.title'));

        $this->color('warning');

        $this->icon('heroicon-s-archive-box');

        $this->requiresConfirmation();

        $this->modalIcon('heroicon-s-archive-box');

        // Checked to here








        $this->hidden(static function (Model $record): bool {
            if (! method_exists($record, 'trashed')) {
                return false;
            }

            return $record->trashed();
        });

        $this->action(function (): void {
            $result = $this->process(static fn (Model $record) => $record->delete());

            if (! $result) {
                $this->failure();

                return;
            }

            $this->success();
        });
    }
}
