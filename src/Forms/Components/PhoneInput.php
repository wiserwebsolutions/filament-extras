<?php

namespace FilamentExtras\Forms\Components;

use Illuminate\Support\Str;

class PhoneInput extends \Filament\Forms\Components\TextInput
{
    protected function setUp(): void
    {
        parent::setUp();

        $this
            ->integer()
            ->inputMode('text')
            ->extraInputAttributes(['style' => '-webkit-appearance: none; -moz-appearance:textfield;'])
            ->prefix('+1')
            ->formatStateUsing(fn (?string $state): ?string => Str::replaceStart('+1', '', $state))
            ->maxLength(10);
    }
}
