<?php

namespace FilamentExtras\Forms\Components;

use Closure;
use Filament\Support\Enums\ActionSize;

class ToggleButtons extends \Filament\Forms\Components\ToggleButtons
{
    use \Filament\Actions\Concerns\HasSize;

    public const GROUPED_VIEW = 'filament-extras::components.toggle-buttons.grouped';

    /**
     * @var view-string
     */
    protected string $view = 'filament-extras::components.toggle-buttons.index';
}
