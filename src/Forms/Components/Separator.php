<?php

namespace FilamentExtras\Forms\Components;

use Closure;
use Filament\Support\Concerns;
use Filament\Support\Enums\Alignment;

class Separator extends \Filament\Forms\Components\Component
{
    use Concerns\HasColor;
    use Concerns\HasIcon;
    use \Filament\Forms\Components\Concerns\HasLabel;

    protected string $view = 'filament-extras::components.separator';

    protected Alignment | string | Closure | null $labelAlignment = Alignment::Center;

    protected string | Closure | null $text = null;

    public static function make(): static
    {
        return app(static::class);
    }

    protected function setUp(): void
    {
        parent::setUp();

        $this->defaultColor('gray');
        $this->hiddenLabel(true);
        $this->dehydrated(false);
    }

    public function labelAlignment(Alignment | string | Closure | null $labelAlignment): static
    {
        $this->labelAlignment = $labelAlignment;

        return $this;
    }

    public function alignLabelLeft(bool | Closure $condition = true): static
    {
        return $this->labelAlignment(fn (): ?Alignment => $this->evaluate($condition) ? Alignment::Start : null);
    }

    public function alignLabelCenter(bool | Closure $condition = true): static
    {
        return $this->labelAlignment(fn (): ?Alignment => $this->evaluate($condition) ? Alignment::Center : null);
    }

    public function alignLabelRight(bool | Closure $condition = true): static
    {
        return $this->labelAlignment(fn (): ?Alignment => $this->evaluate($condition) ? Alignment::End : null);
    }

    public function getLabelAlignment(): Alignment | string | null
    {
        return $this->evaluate($this->labelAlignment);
    }
}
