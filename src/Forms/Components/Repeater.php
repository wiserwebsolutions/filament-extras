<?php

namespace FilamentExtras\Forms\Components;

use Closure;

class Repeater extends \Filament\Forms\Components\Repeater
{
    use \Filament\Forms\Components\Concerns\CanBeCompacted;

    protected bool | Closure | null $isCollapsible = false;

    /**
     * @return view-string
     */
    public function getDefaultView(): string
    {
        if ($this->isSimple()) {
            return 'filament-forms::components.repeater.simple';
        }

        if ($this->isCompact()) {
            return 'filament-extras::components.repeater.compact';
        }

        return 'filament-forms::components.repeater.index';
    }
}
