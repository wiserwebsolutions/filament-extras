@php
    $color = $getColor();
    $label = $getLabel();
    $labelAlignment = $getLabelAlignment();
@endphp

<div class="relative">
    <div class="absolute inset-0 flex items-center" aria-hidden="true">
        <div class="w-full border-t border-{{ $color }}-300"></div>
    </div>
    @unless ($isLabelHidden())
        <div class="relative flex justify-{{ $labelAlignment }}">
            <span class="px-2 text-sm text-{{ $color }}-500 bg-white">{{ $label }}</span>
        </div>
    @endunless
</div>
