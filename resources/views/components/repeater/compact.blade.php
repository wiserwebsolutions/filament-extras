@php
    use Filament\Forms\Components\Actions\Action;

    $containers = $getChildComponentContainers();

    $addAction = $getAction($getAddActionName());
    $addBetweenAction = $getAction($getAddBetweenActionName());
    $cloneAction = $getAction($getCloneActionName());
    $collapseAllAction = $getAction($getCollapseAllActionName());
    $expandAllAction = $getAction($getExpandAllActionName());
    $deleteAction = $getAction($getDeleteActionName());
    $moveDownAction = $getAction($getMoveDownActionName());
    $moveUpAction = $getAction($getMoveUpActionName());
    $reorderAction = $getAction($getReorderActionName());
    $extraItemActions = $getExtraItemActions();

    $isAddable = $isAddable();
    $isCloneable = $isCloneable();
    $isCollapsible = $isCollapsible();
    $isDeletable = $isDeletable();
    $isReorderableWithButtons = $isReorderableWithButtons();
    $isReorderableWithDragAndDrop = $isReorderableWithDragAndDrop();

    $statePath = $getStatePath();
@endphp

<x-dynamic-component :component="$getFieldWrapperView()" :field="$field">
    <div
        x-data="{}"
        {{ $attributes->merge($getExtraAttributes(), escape: false)->class(['fi-fo-repeater grid gap-y-4']) }}>
        @if ($isCollapsible && ($collapseAllAction->isVisible() || $expandAllAction->isVisible()))
            <div
                @class(['flex gap-x-3', 'hidden' => count($containers) < 2])>
                @if ($collapseAllAction->isVisible())
                    <span
                        x-on:click="$dispatch('repeater-collapse', '{{ $statePath }}')">
                        {{ $collapseAllAction }}
                    </span>
                @endif

                @if ($expandAllAction->isVisible())
                    <span
                        x-on:click="$dispatch('repeater-expand', '{{ $statePath }}')">
                        {{ $expandAllAction }}
                    </span>
                @endif
            </div>
        @endif

        @if (count($containers))
            <ul>
                <x-filament::grid
                    :default="$getGridColumns('default')"
                    :sm="$getGridColumns('sm')"
                    :md="$getGridColumns('md')"
                    :lg="$getGridColumns('lg')"
                    :xl="$getGridColumns('xl')"
                    :two-xl="$getGridColumns('2xl')"
                    :wire:end.stop="'mountFormComponentAction(\'' . $statePath . '\', \'reorder\', { items: $event.target.sortable.toArray() })'"
                    x-sortable
                    :data-sortable-animation-duration="$getReorderAnimationDuration()"
                    class="items-start gap-4">
                    @foreach ($containers as $uuid => $item)
                        @php
                            $itemLabel = $getItemLabel($uuid);
                            $visibleExtraItemActions = array_filter($extraItemActions, fn(Action $action): bool => $action(['item' => $uuid])->isVisible());
                        @endphp

                        <li
                            wire:key="{{ $this->getId() }}.{{ $item->getStatePath() }}.{{ $field::class }}.item"
                            x-data="{
                                isCollapsed: @js($isCollapsed($item)),
                            }"
                            x-on:expand="isCollapsed = false"
                            x-on:repeater-expand.window="$event.detail === '{{ $statePath }}' && (isCollapsed = false)"
                            x-on:repeater-collapse.window="$event.detail === '{{ $statePath }}' && (isCollapsed = true)"
                            x-sortable-item="{{ $uuid }}"
                            class="bg-white shadow-sm fi-fo-repeater-item rounded-xl ring-1 ring-gray-950/5 dark:bg-white/5 dark:ring-white/10">

                            <div class="flex items-center justify-between w-full gap-x-2">
                                @if ($isReorderableWithDragAndDrop || $isReorderableWithButtons)
                                    {{-- START LEFT COMMAND COLUMN --}}
                                    <div class="flex flex-col content-between px-3 py-2">
                                        @if ($isReorderableWithDragAndDrop)
                                            <div class="inline-flex items-center justify-center size-7 gap-x-2 rounded-se-lg disabled:opacity-50 disabled:pointer-events-none"
                                                x-sortable-handle
                                                x-on:click.stop>
                                                {{ $reorderAction }}
                                            </div>
                                        @endif

                                        @if ($isReorderableWithButtons)
                                            <div
                                                x-on:click.stop
                                                class="flex items-center justify-center">
                                                {{ $moveUpAction(['item' => $uuid])->disabled($loop->first) }}
                                            </div>

                                            <div
                                                x-on:click.stop
                                                class="flex items-center justify-center">
                                                {{ $moveDownAction(['item' => $uuid])->disabled($loop->last) }}
                                            </div>
                                        @endif
                                    </div>
                                    {{-- END LEFT COMMAND COLUMN --}}
                                @endif

                                <div class="px-3 py-2 grow">
                                    {{ $item }}
                                </div>

                                @if ($isCloneable || $isDeletable || count($visibleExtraItemActions))
                                    {{-- START RIGHT COMMAND COLUMN --}}
                                    <div class="flex flex-col content-between px-3 py-2">
                                        @if ($isDeletable)
                                            <div class="inline-flex items-center justify-center size-7 gap-x-2 rounded-se-lg disabled:opacity-50 disabled:pointer-events-none"
                                                x-on:click.stop>
                                                {{ $deleteAction(['item' => $uuid]) }}
                                            </div>
                                        @endif

                                        @if ($isCloneable)
                                            <div class="inline-flex items-center justify-center size-7 gap-x-2 rounded-se-lg disabled:opacity-50 disabled:pointer-events-none"
                                                x-on:click.stop>
                                                {{ $cloneAction(['item' => $uuid]) }}
                                            </div>
                                        @endif
                                    </div>
                                    {{-- END RIGHT COMMAND COLUMN --}}
                                @endif
                            </div>
                        </li>

                        @if (!$loop->last)
                            @if ($isAddable && $addBetweenAction->isVisible())
                                <li class="flex justify-center w-full">
                                    <div
                                        class="bg-white rounded-lg fi-fo-repeater-add-between-action-ctn dark:bg-gray-900">
                                        {{ $addBetweenAction(['afterItem' => $uuid]) }}
                                    </div>
                                </li>
                            @elseif (filled($labelBetweenItems = $getLabelBetweenItems()))
                                <li
                                    class="relative border-t border-gray-200 dark:border-white/10">
                                    <span
                                        class="absolute px-1 text-sm font-medium -top-3 left-3">
                                        {{ $labelBetweenItems }}
                                    </span>
                                </li>
                            @endif
                        @endif
                    @endforeach
                </x-filament::grid>
            </ul>
        @endif

        @if ($isAddable)
            <div class="flex justify-center">
                {{ $addAction }}
            </div>
        @endif
    </div>
</x-dynamic-component>
