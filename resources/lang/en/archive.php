<?php

return [

    'single' => [

        'label' => 'Archive',

        'modal' => [

            'heading' => 'Archive :label',

            'actions' => [

                'archive' => [
                    'label' => 'Archive',
                ],

            ],

        ],

        'notifications' => [

            'archived' => [
                'title' => 'Archived',
            ],

        ],

    ],

    'multiple' => [

        'label' => 'Archive selected',

        'modal' => [

            'heading' => 'Archive selected :label',

            'actions' => [

                'archive' => [
                    'label' => 'Archive',
                ],

            ],

        ],

        'notifications' => [

            'archived' => [
                'title' => 'Archived',
            ],

        ],

    ],

];
